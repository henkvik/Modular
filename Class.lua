local _, g = ...;

local Class = {};
function Class:create(class, obj)
    obj.parent = self;
    return g.create(class, obj);
end

function Class:__index(key)
  if self.class[key] ~= nil then return self.class[key]; end

  local super = self.class.super;
  while super do
    if super[key] ~= nil then return super[key]; end
    super = super.super;
  end
end

local classes = {};

function g.create(class, obj)
    if classes[class] == nil then
      error(class .. " is not a valid class"); 
    end

    obj = obj or {};
    obj.class = classes[class];
    setmetatable(obj, Class);
    
    obj:init();

    return obj;
end

function g.class(class, super)
    if super ~= nil and classes[super] == nil then
      error(super .. " is not a valid class"); 
    end

    classes[class] = {};
    classes[class].super = classes[super] or Class;    

    return classes[class];
end
