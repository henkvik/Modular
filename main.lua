local _, g = ...;

UIParentLoadAddOn("Blizzard_DebugTools");
tinspect = DisplayTableInspectorWindow;


local tooltip_border = "Interface\\Tooltips\\UI-Tooltip-Border.blp";
local solid_white    = "Interface\\BUTTONS\\WHITE8X8.BLP";
local bar_texture    = "Interface\\TARGETINGFRAME\\BarFill2.blp";

local Modular = LibStub("AceAddon-3.0"):NewAddon("Modular", "AceConsole-3.0", "AceEvent-3.0");




function Modular:OnInitialize()
    
    local minimap = g.create("MinimapPanel", {
        background = { color = { a = 0.75 }, offset = 4 },
        points = {{ point = "CENTER", y = -400 }},
    });

    local player_panel = g.create("UnitPanel", {
        name = "PlayerPanel",
        background = { color = { a = 0.75 }, offset = 4 },
        width = 200,
        height = 50,
        unit = "player",
        points = {{ point = "TOPRIGHT", relative = minimap, r_point = "TOPLEFT", x = -12 }},
    });

    player_panel:create("HealthBar", {
        name = "PlayerHealthBar",
        reverse = true,
        texture = g.media.bars.Smoov,
        width = 200,
        height = 50,
        points = {{ point = "TOPRIGHT" }},
        color = { r = 1, g = 1, b = 1 }
    });

    local target_panel = g.create("UnitPanel", {
        background = { color = { a = 0.75 }, offset = 4 },
        width = 200,
        height = 50,
        unit = "target",
        points = {{ point = "TOPLEFT", relative = minimap, r_point = "TOPRIGHT", x = 12 }},
    });

    target_panel:create("HealthBar", {
        name = "TargetHealthBar",
        texture = g.media.bars.Smoov,
        width = 200,
        height = 50,
        points = {{ point = "TOPRIGHT" }},
        color = { r = 1, g = 1, b = 1 }
    });
    
end

function Modular:OnEnable()
end

function Modular:OnDisable()
end