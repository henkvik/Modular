local _, g = ...;

local path = "Interface\\Addons\\!Modular\\Media\\";

g.media = g.media or {};

g.media.borders = {
    BorderBase = path .. "Border-Base.tga"
};

g.media.bars = {
    Smoov = path .. "Smoov.tga"
};