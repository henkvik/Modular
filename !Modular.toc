## Interface: 70300
## Title: Modular
## Notes: Extremly Modular User Interface Replacment
## Version: 0.0.1
## Author: Henrik

# Dependencies
Include.xml

# Helpers
Class.lua
Media/!Media.lua

# Modules
Modules/Panel.lua
Modules/UnitPanel.lua
Modules/Minimap.lua

Modules/ProgressBar.lua
Modules/HealthBar.lua
Modules/PowerBar.lua
Modules/CastBar.lua

Modules/ActionButton.lua
Modules/ActionBar.lua

# Entrypoint
main.lua