local _, g = ...;

local ProgressBar = g.class("ProgressBar", "Panel");

function ProgressBar:init()
    self.min = self.min or 0;
    self.max = self.max or 1;

    ProgressBar.super.init(self);

    local f = self.frame;
    self.bar = f:CreateTexture((self.name and self.name .. "Bar") or nil, "ARTWORK");
    self.bar:SetTexture(self.texture or "Interface\\BUTTONS\\WHITE8X8.BLP", true);
    self.bar:SetHorizTile(true);
    self.bar:SetHeight(self.height);
    self.bar:SetWidth(0.00001);

    local color = self.color or {};
    self:set_color(color.r, color.g, color.b, color.a);

    if self.reverse then
        self.bar:SetPoint("RIGHT", f);
    else
        self.bar:SetPoint("LEFT", f);
    end
end

function ProgressBar:set_value(value)
    self.value = value;
    local width = max(0, min(1, (self.value - self.min) / self.max)) * self.width;
    self.bar:SetWidth(max(width,0.00001));
end

function ProgressBar:set_color(r, g, b, a)
    self.bar:SetVertexColor(
        r or 0,
        g or 0,
        b or 0,
        a or 1
    );
end
