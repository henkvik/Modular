local _, g = ...;

local HealthBar = g.class("HealthBar", "ProgressBar");

function HealthBar:init()
    self.unit = self.unit or (self.parent and self.parent.unit);
    if not self.unit then error("unit property required"); end

    self.events = {
        "PLAYER_ENTERING_WORLD",
        "PLAYER_TARGET_CHANGED", 
        "UNIT_HEALTH",
        "UNIT_HEAL_PREDICTION",
        "UNIT_ABSORB_AMOUNT_CHANGED",
        "UNIT_HEAL_ABSORB_AMOUNT_CHANGED",
    };

    HealthBar.super.init(self); -- RUN SUPER CONSTRUCTOR


    self.absorb = self:create("ProgressBar", {
        name = self.name .. "Absorb",
        height = self.height,
        width = self.width,
        reverse = self.reverse,
        texture = self.texture,
        level = 3,
        color = { g = 0.66, b = 1, a = 0.8 },
        points = {{ 
            point = (self.reverse and "RIGHT") or "LEFT",
            relative = self.frame 
        }},
    });

    self.heal_absorb = self:create("ProgressBar", {
        name = self.name .. "HealAbsorb",
        height = self.height,
        width = self.width,
        texture = self.texture,
        reverse = not self.reverse,
        level = 5,
        color = { r = 1, g = 0.33, a = 0.8 },
        points = {{ 
            point = (self.reverse and "LEFT") or "RIGHT", 
            r_point = (self.reverse and "LEFT") or "RIGHT", 
            relative = self.bar
        }},
    });

    self.heal = self:create("ProgressBar", {
        name = self.name .. "Heal",
        height = self.height,
        width = self.width,
        reverse = self.reverse,
        texture = self.texture,
        level = 4,
        color = { r = 0.66, g = 1, a = 0.8 },
        points = {{ 
            point = (self.reverse and "RIGHT") or "LEFT", 
            r_point = (self.reverse and "RIGHT") or "LEFT", 
            relative = self.heal_absorb.bar 
        }},
    });

end

function HealthBar:on_event(e, ...)
    local unit = ...;    
    
    if e == "PLAYER_ENTERING_WORLD" and self.unit == "player" then
        local r, g, b = GetClassColor(select(2, UnitClass(self.unit)));
        self:set_color(r, g, b);

        self:update_health();
        self:update_absorb();
        self:update_heal_absorb();
        self:update_heal();
    end

    if e == "PLAYER_TARGET_CHANGED" and self.unit == "target" then
        local r, g, b;
        if UnitIsPlayer(self.unit) then
            r, g, b = GetClassColor(select(2, UnitClass(self.unit)));
        else
            r, g, b = UnitSelectionColor(self.unit)
            r = r * 0.6 + 0.2;
            g = g * 0.6 + 0.2;
            b = b * 0.6 + 0.2;
        end
        self:set_color(r, g, b);

        self:update_health();
        self:update_absorb();
        self:update_heal_absorb();
        self:update_heal();
    end

    if unit == self.unit then
        if e == "UNIT_HEALTH" then
            self:update_health();
            self:update_heal();
        end

        if "UNIT_ABSORB_AMOUNT_CHANGED" then
            self:update_absorb();
        end

        if e == "UNIT_HEAL_PREDICTION" then
            self:update_heal();
        end

        if e == "UNIT_HEAL_ABSORB_AMOUNT_CHANGED" then
            self:update_heal_absorb();
        end
    end
end

function HealthBar:update_health()
    local health = UnitHealth(self.unit) / UnitHealthMax(self.unit);
    self:set_value(health);
end

function HealthBar:update_absorb()
    local absorb = UnitGetTotalAbsorbs(self.unit) / UnitHealthMax(self.unit);
    print(absorb, UnitGetTotalAbsorbs(self.unit), UnitHealthMax(self.unit));
    self.absorb:set_value(absorb);
end

function HealthBar:update_heal_absorb()
    local heal_absorb = UnitGetTotalHealAbsorbs(self.unit) / UnitHealthMax(self.unit);
    self.heal_absorb:set_value(heal_absorb);
end

function HealthBar:update_heal()
    local heal = (UnitGetIncomingHeals(self.unit) or 0) / UnitHealthMax(self.unit);
    heal = min(heal, 1 - self.value);
    self.heal:set_value(heal);
end