local _, g = ...;

local Panel = g.class("Panel");

function Panel:init()
    self:create_frame();

    self:set_points(self.points or {
        { point = "CENTER" }
    });

    if self.background then self:set_background(self.background); end
    if self.border then self:set_border(self.border); end


    if self.events then
        self.frame:SetScript("OnEvent", function(...) self:on_event(select(2, ...)) end);
        for _,event in ipairs(self.events or {}) do
            self.frame:RegisterEvent(event);
        end
    end
end

function Panel:create_frame()
    local parent = self.parent and self.parent.frame or self.parent or UIParent;
    self.strata = self.strata or (self.parent and self.parent.strata) or "MEDIUM";
    self.level = self.level or (self.parent and (self.parent.level + 1)) or 0;
    self.width = self.width or 100;
    self.height = self.height or 100;

    self.frame = CreateFrame(self.type or "FRAME", self.name, parent, self.template);
    self.frame:SetFrameStrata(self.strata);
    self.frame:SetFrameLevel(self.level);
    self.frame:SetWidth(self.width);
    self.frame:SetHeight(self.height);
    self.frame:Show();

    self.frame.self = self;
end

function Panel:set_points(points)
    self.frame:ClearAllPoints();

    for i,p in ipairs(points) do
        local parent = self.parent and self.parent.frame or self.parent or UIParent;
        local relative = p.relative and p.relative.frame or p.relative or parent;
        self.frame:SetPoint(
            p.point,
            relative,
            p.r_point or p.point,
            p.x or 0,
            p.y or 0
        );
    end    
end

function Panel:set_background(background)
    local f = self.frame;

    if f.background == nil then
        f.background = f:CreateTexture(
            self.name and self.name .. "Background" or nil,
            background.layer
        );
    end

    f.background:SetTexture(background.texture);

    local color = background.color or {};
    f.background:SetColorTexture(
        color.r or 0,
        color.g or 0,
        color.b or 0,
        color.a or 1
    );

    local offset = background.offset or {};
    if type(offset) == "number" then 
        offset = { all = offset };
    end

    f.background:SetPoint("TOPLEFT", 
        (offset.left or 0) - (offset.all or 0),
        (offset.top  or 0) + (offset.all or 0)
    );
    f.background:SetPoint("BOTTOMRIGHT", 
        (offset.right  or 0) + (offset.all or 0),
        (offset.bottom or 0) - (offset.all or 0)
    );
end

function Panel:set_border(border)
    local f = self.frame;

    if f.border == nil then
        f.border = CreateFrame("FRAME", self.name and self.name .. "Border" or nil, f);
        f.border:SetFrameStrata(f:GetFrameStrata());
        f.border:SetFrameLevel(f:GetFrameLevel() + 1);
    end

    f.border:SetBackdrop({
        edgeFile = border.texture,
        edgeSize = border.size or 16
    });

    local color = border.color or { r = 1, g = 1, b = 1};
    f.border:SetBackdropBorderColor(
        color.r or 0, 
        color.g or 0, 
        color.b or 0, 
        color.a or 1
    );

    local offset = border.offset or {};
    if type(offset) == "number" then 
        offset = { all = offset };
    end

    f.border:SetPoint("TOPLEFT", 
        (offset.left or 0) - (offset.all or 0),
        (offset.top  or 0) + (offset.all or 0)
    );
    f.border:SetPoint("BOTTOMRIGHT", 
        (offset.right  or 0) + (offset.all or 0),
        (offset.bottom or 0) - (offset.all or 0)
    );
end

function Panel:set_draggable(draggable)

    self.frame:SetMovable(draggable);
    self.frame:EnableMouse(draggable);
    self.frame:RegisterForDrag("LeftButton");
    self.frame:SetScript("OnDragStart", self.frame.StartMoving);
    self.frame:SetScript("OnDragStop", 
        function(frame)
            frame:StopMovingOrSizing();
            local point, frame, r_point, x, y = frame:GetPoint(0);

            self.points = self.points or {};
            table.wipe(self.points);
            table.insert(self.points, {
                point = point,
                frame = frame,
                r_point = r_point,
                x = x,
                y = y
            });
        end
    );

end