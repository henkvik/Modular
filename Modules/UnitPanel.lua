local _, g = ...;

local UnitPanel = g.class("UnitPanel", "Panel");

function UnitPanel:init()
    if not self.unit then error("unit property is required"); end
    self.type = "Button";
    self.template = "SecureUnitButtonTemplate";
    UnitPanel.super.init(self);

    local f = self.frame;
    f:RegisterForClicks("AnyUp");
    f:SetAttribute("unit", self.unit);
    f:SetAttribute("*type1", "target");
    f:SetAttribute("*type2", "menu");
    RegisterUnitWatch(f);
    
end
