local _, g = ...;

local MinimapPanel = g.class("MinimapPanel", "Panel");

function MinimapPanel:init()
    self.width = self.width or 150;
    self.height = self.width;
    
    self.strata = self.strata or "LOW";
    self.level = self.level or 0;

    self.events = { "PLAYER_ENTERING_WORLD" };

    self.super.init(self);

    self:move_bliz_frames();

    MinimapZoneTextButton:Hide(); 
    Minimap:SetScript("OnEnter", function()
        MinimapZoneTextButton:Show(); 
    end);

    Minimap:SetScript("OnLeave", function()
        MinimapZoneTextButton:Hide(); 
    end);

end

function MinimapPanel:move_bliz_frames()
    MinimapCluster:ClearAllPoints();
    MinimapCluster:SetPoint("TOPLEFT", self.frame, "TOPLEFT");
    MinimapCluster:SetPoint("BOTTOMRIGHT", self.frame, "BOTTOMRIGHT");
    
    Minimap:SetHeight(self.width);
    Minimap:SetWidth(self.height);
    Minimap:SetAllPoints(MinimapCluster);
    Minimap:SetMaskTexture("Interface\\BUTTONS\\WHITE8X8.BLP");

    Minimap:SetArchBlobRingScalar(0);
    Minimap:SetQuestBlobRingScalar(0);
    Minimap:SetTaskBlobRingScalar(0);

	Minimap:EnableMouseWheel(true)
    Minimap:SetScript("OnMouseWheel", function(_, d)
        Minimap:SetZoom(max(0,Minimap:GetZoom() + d));
    end)
    
    MinimapBackdrop:SetAllPoints(MinimapCluster);

    MinimapZoneTextButton:ClearAllPoints();
    MinimapZoneTextButton:SetPoint("TOP", MinimapCluster);
    
    MinimapBorderTop:Hide();
    MinimapNorthTag:Hide();
    MinimapBorder:Hide();
    

    -- ObjectiveTrackerFrame:ClearAllPoints();
    -- ObjectiveTrackerFrame:SetPoint("TOPRIGHT", UIParent);
    -- ObjectiveTrackerFrame:SetPoint("BOTTOM", UIParent);


    -- LibStub("AceEvent-3.0"):RegisterEvent("QUEST_LOG_UPDATE", function()
    --     ObjectiveTrackerFrame:ClearAllPoints();
    --     ObjectiveTrackerFrame:SetPoint("TOPRIGHT", UIParent);
    --     ObjectiveTrackerFrame:SetPoint("BOTTOM", UIParent);
    -- end);



end

function MinimapPanel:manage_buttons()
    self.hidden_buttons = {
        MiniMapWorldMapButton,
        MinimapZoomIn,
        MinimapZoomOut
    };

    for _,button in ipairs(self.hidden_buttons) do
        button:Hide();
    end


    self.button_anchor = self:create("Panel", {
        background = { color = { a = 0.75} },
        width = 32,
        height = 8,
        points = {{ point = "RIGHT", relative = UIParent }}
    });
    self.button_anchor:set_draggable(true);
    self.button_anchor.frame:SetClampedToScreen(true);
    local i = 0;


    self.buttons = {
        GameTimeFrame,
        MiniMapTracking,
        GarrisonLandingPageMinimapButton,
    };

    for _,button in ipairs(self.buttons) do
        button:SetHeight(32);
        button:SetWidth(32);
        button:ClearAllPoints();
        button:SetPoint("TOP", self.button_anchor.frame, "BOTTOM", 0, -(i * 32));
        i = i + 1;
    end

    self.addon_buttons = LibStub:GetLibrary("LibDBIcon-1.0").objects or {};
    
    for _,button in pairs(self.addon_buttons) do
        button:SetHeight(32);
        button:SetWidth(32);
        button:ClearAllPoints();
        button:SetPoint("TOP", self.button_anchor.frame, "BOTTOM", 0, -(i * 32));
        i = i + 1;
    end

    self.dynamic_buttons = {
        QueueStatusMinimapButton,
        MiniMapMailFrame
    };

    for i,button in ipairs(self.dynamic_buttons) do
        button:SetHeight(32);
        button:SetWidth(32);
        button:ClearAllPoints();
        button:SetPoint("TOPLEFT", Minimap, "TOPLEFT", -16, -((i-1) * 32));
    end

    TimeManagerClockButton:ClearAllPoints();
    TimeManagerClockButton:SetPoint("CENTER", Minimap, "BOTTOM", 0, -2);
end

function MinimapPanel:on_event()
    self:manage_buttons();   
end